import React from 'react'
import ReactDOM from 'react-dom'
import TodoList from './components/todo-list'
import SearchPanel from './components/search-panel'
import HeaderApp from './components/header-app'
import ItemStatusFilter from './components/item-status-filter'

const App = () => {
  const todoData = [
    { label: 'Drink Coffee', important: false, id: 1 },
    { label: 'Make Awesome App', important: true, id: 2 },
    { label: 'Eat a lunch', important: false, id: 3 },
  ]
  return (
    <div className="container">
      <HeaderApp toDo="3" done="1" />
      <SearchPanel />
      <ItemStatusFilter />
      <TodoList todos={todoData} />
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
