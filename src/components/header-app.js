import React from 'react'
import './header-app.css'

const HeaderApp = ({ toDo, done }) => {
  
  return (
    <div className="app-header d-flex">
      <h1>TODO List</h1>
      <h2>
        {toDo} more to do, {done} done
      </h2>
    </div>
  )
}

export default HeaderApp
